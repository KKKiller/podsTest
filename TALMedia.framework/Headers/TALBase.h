#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, TALMediaType) {
    TALMediaTypeUnknown = 0,
    TALMediaTypeVideo,
    TALMediaTypeAudio,
    TALMediaTypeText,
};

typedef NS_ENUM(NSInteger, TALCameraPosition) {
    TALCameraPositionBack  = 1,
    TALCameraPositionFront = 2,
};

/*
 ________________________________________
 | Standard | Resolution | Aspect Ratio |
 ----------------------------------------
 |   720p   |  1280x720  |    16:9      |
 ----------------------------------------
 |          |  720x1280  |     9:16     |
 ----------------------------------------
 |   480p   |   640x480  |     4:3      |
 ----------------------------------------
 |          |   480x640  |     3:4      |
 ----------------------------------------
 | Wide 360p|   640x360  |    16:9      |
 ----------------------------------------
 |          |   360x640  |     9:16     |
 ----------------------------------------
 |   360p   |   480x360  |     4:3      |
 ----------------------------------------
 |          |   360x480  |     3:4      |
 ----------------------------------------
 |   288p   |   352x288  |     4:3      |
 ----------------------------------------
 |          |   288x352  |     3:4      |
 ----------------------------------------
 |   180p   |   320x180  |    16:9      |
 ----------------------------------------
 |          |   180x320  |     9:16     |
 ----------------------------------------
 
 ________________________________________
 |  Custom  | Resolution | Aspect Ratio |
 ----------------------------------------
 |          |   720x720  |     1:1      |
 ----------------------------------------
 |          |   480x480  |     1:1      |
 ----------------------------------------
 |          |   360x360  |     1:1      |
 ----------------------------------------
 |          |   288x288  |     1:1      |
 ----------------------------------------
 |          |   180x180  |     1:1      |
 ----------------------------------------
 */
typedef NS_ENUM(NSInteger, TALVideoResolution) {
    TALVideoResolution720p = 0,
    TALVideoResolution480p,
    TALVideoResolutionWide360p,
    TALVideoResolution360p,
    TALVideoResolution288p,
    TALVideoResolution180p,
    
    // Square
    TALVideoResolution720x720,
    TALVideoResolution480x480,
    TALVideoResolution360x360,
    TALVideoResolution288x288,
    TALVideoResolution180x180,
};

typedef NS_ENUM(NSInteger, TALViewContentMode) {
    TALViewContentModeScaleAspectFit = 0,
    TALViewContentModeScaleAspectFill,
};

typedef NS_ENUM(NSInteger, TALRecordType) {
    TALRecordTypeMP4 = 0,
    TALRecordTypeFLV,
};
