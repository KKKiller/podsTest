#import <Foundation/Foundation.h>

// Codes that specify TALPlayer status.
typedef NS_ENUM(NSInteger, TALMediaPlayerStatus) {
    TALMediaPlayerStatusStarted = 0,
    TALMediaPlayerStatusStartBuffering,
    TALMediaPlayerStatusStopBuffering,
    TALMediaPlayerStatusStopped,
    TALMediaPlayerStatusVideoSize,
    TALMediaPlayerStatusNetworkConnected,
    TALMediaPlayerStatusFirstPacket,
};

// Codes that specify TALPublisher status.
typedef NS_ENUM(NSInteger, TALMediaPublisherStatus) {
    TALMediaPublisherStatusPreviewStarted = 0,
    TALMediaPublisherStatusPreviewStopped,
    
    TALMediaPublisherStatusPublishStarted,
    TALMediaPublisherStatusPublishStopped,
    
    TALMediaPublisherStatusRecordStarted,
    TALMediaPublisherStatusRecordStopped,
};
