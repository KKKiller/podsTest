#import <Foundation/Foundation.h>
#import <TALMedia/TALBase.h>

@interface TALMediaPlayerStatistics : NSObject

@property (nonatomic, assign) TALMediaType mediaType;

@property (nonatomic, strong) NSNumber* videoWidth; // int
@property (nonatomic, strong) NSNumber* videoHeight; // int
@property (nonatomic, strong) NSNumber* videoRecvBytes; // long long
@property (nonatomic, strong) NSNumber* videoRecvFrames; // long long
@property (nonatomic, strong) NSNumber* videoDecFrames; // long long

@property (nonatomic, strong) NSNumber* audioChannel; // int
@property (nonatomic, strong) NSNumber* audioSampleRate; // int
@property (nonatomic, strong) NSNumber* audioRecvBytes; // long long
@property (nonatomic, strong) NSNumber* audioRecvFrames; // long long
@property (nonatomic, strong) NSNumber* audioDecFrames; // long long

@property (nonatomic, strong) NSNumber* curBufferingTime; // double, second
@property (nonatomic, strong) NSNumber* maxBufferingTime; // double, second

@end

@interface TALMediaPublisherStatistics : NSObject

@property (nonatomic, assign) TALMediaType mediaType;

@property (nonatomic, strong) NSNumber* videoWidth; // int
@property (nonatomic, strong) NSNumber* videoHeight; // int
@property (nonatomic, strong) NSNumber* captureWidth; // int
@property (nonatomic, strong) NSNumber* captureHeight; // int
@property (nonatomic, strong) NSNumber* videoInputFrames; // long long
@property (nonatomic, strong) NSNumber* videoEncFrames; // long long
@property (nonatomic, strong) NSNumber* videoEncBytes; // long long
@property (nonatomic, strong) NSNumber* videoSendBytes; // long long

@property (nonatomic, strong) NSNumber* audioChannel; // int
@property (nonatomic, strong) NSNumber* audioSampleRate; // int
@property (nonatomic, strong) NSNumber* audioInputFrames; // long long
@property (nonatomic, strong) NSNumber* audioEncFrames; // long long
@property (nonatomic, strong) NSNumber* audioEncBytes; // long long
@property (nonatomic, strong) NSNumber* audioSendBytes; // long long

@end
