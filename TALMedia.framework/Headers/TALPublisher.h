#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import <TALMedia/TALEvent.h>
#import <TALMedia/TALStatus.h>
#import <TALMedia/TALStatistics.h>

@class TALContext;
@protocol TALPublisherDelegate;

@interface TALPublisher : NSObject

- (instancetype)initWithContext:(TALContext *)context;

- (BOOL)addDelegate:(id<TALPublisherDelegate>)delegate delegateQueue:(dispatch_queue_t)queue;
- (void)removeDelegate:(id<TALPublisherDelegate>)delegate;
- (void)removeAllDelegates;

- (NSUInteger)getCameraCount;

// position: default TALCameraPositionFront
- (BOOL)setCameraPosition:(TALCameraPosition)position;

// resolution: default TALVideoResolution480p
// frameRate: [1, 30], default 15
- (void)setCaptureResolution:(TALVideoResolution)resolution frameRate:(NSUInteger)frameRate;

// resolution: default TALVideoResolution480p
// frameRate: [1, 30], default 15
- (void)setEncodeResolution:(TALVideoResolution)resolution frameRate:(NSUInteger)frameRate;

// default GOP 4 seconds
- (void)setGOPInterval:(NSUInteger)second;

// default 512000
- (void)setBitRate:(NSUInteger)bitRate;

// default false
- (void)enableBeauty:(BOOL)enable;

- (UIView *)createPreviewViewWithFrame:(CGRect)frame;
- (void)destroyPreviewView;
- (UIView *)getPreviewView;
- (void)setPreviewViewContentMode:(TALViewContentMode)mode; // default TALViewContentModeScaleAspectFit

- (void)setPreviewMute:(BOOL)mute;
- (BOOL)isPreviewMuted;
- (BOOL)startPreview;
- (void)stopPreview;

// default TALRecordTypeMP4
// MUST call this method before startPush if you want to record.
- (BOOL)setRecordType:(TALRecordType)type;

// support file with the mp4 or flv extension
- (BOOL)setRecordFile:(NSString *)path;
- (BOOL)startRecord;
- (void)stopRecord;

- (BOOL)addPushURL:(NSURL *)url audioOnly:(BOOL)audioOnly;
- (BOOL)removePushURL:(NSURL *)url;
- (void)removeAllPushURLs;
- (BOOL)startPush;
- (void)stopPush;

// You can invoke this method while the TALPublisher is publishing.
- (BOOL)controlPushURL:(NSURL *)url audioOnly:(BOOL)audioOnly;

// MUST run in a non-main thread
- (NSArray<TALMediaPublisherStatistics *> *)getStatistics;

@end

@protocol TALPublisherDelegate <NSObject>

- (void)onPublisher:(TALPublisher *)publisher error:(NSError *)error;
- (void)onPublisher:(TALPublisher *)publisher event:(TALMediaEvent)event;
- (void)onPublisher:(TALPublisher *)publisher statusChanged:(TALMediaPublisherStatus)status;

@end
