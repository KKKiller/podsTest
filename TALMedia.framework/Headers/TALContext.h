#import <Foundation/Foundation.h>

@interface TALContext : NSObject

- (NSString *)getVersion;
- (BOOL)setLogPath:(NSString *)logPath;

@end
