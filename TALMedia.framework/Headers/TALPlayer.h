#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import <TALMedia/TALStatus.h>
#import <TALMedia/TALStatistics.h>

@class TALContext;
@protocol TALPlayerDelegate;

@interface TALPlayer : NSObject

- (instancetype)initWithContext:(TALContext *)context;

- (BOOL)addDelegate:(id<TALPlayerDelegate>)delegate delegateQueue:(dispatch_queue_t)queue;
- (void)removeDelegate:(id<TALPlayerDelegate>)delegate;
- (void)removeAllDelegates;

- (UIView *)createPlayerViewWithFrame:(CGRect)frame;
- (void)destroyPlayerView;
- (UIView *)getPlayerView;
- (void)setPlayerViewContentMode:(TALViewContentMode)mode; // default TALViewContentModeScaleAspectFit
- (void)clearPlayerViewWhenStop:(BOOL)clear; // default YES

- (BOOL)setURL:(NSURL *)url;
- (void)setMaxBufferingTime:(NSInteger)milliseconds;
- (BOOL)play;
- (void)stop;
- (BOOL)isPlaying;

- (void)setAudioVolume:(NSInteger)volume; // [0, 100], default 100
- (void)muteAudio:(BOOL)mute;
- (BOOL)isAudioMuted;

- (BOOL)beginCoexistWithOtherAudioCapturer:(BOOL)stopAudioPlay;
- (BOOL)endCoexistWithOtherAudioCapturer;

// MUST run in a non-main thread
- (NSArray<TALMediaPlayerStatistics *> *)getStatistics;

@end

@protocol TALPlayerDelegate <NSObject>

- (void)onPlayer:(TALPlayer *)player error:(NSError *)error;
- (void)onPlayer:(TALPlayer *)player statusChanged:(TALMediaPlayerStatus)status;

@end
