#import <Foundation/Foundation.h>

// Codes that specify TALMedia event.
typedef NS_ENUM(NSInteger, TALMediaEvent) {
    TALMediaEventNetworkResume = 0,
};
