//
//  TALCommonDefine.h
//  直播云Demo
//
//  Created by 周吾昆 on 2018/2/24.
//  Copyright © 2018年 培优在线. All rights reserved.
//

typedef NS_ENUM(NSInteger, TALLivePlayerError) {
    // Generic error code
    TALLivePlayerErrorUnknown = -1,
    TALLivePlayerErrorNoError = 0,
    TALLivePlayerErrorInternalError,
    // TALPlayer error code
    TALLivePlayerErrorConnectStreamFailed,//错误地址
    TALLivePlayerErrorBadStream,//老师网络不好或者用户网络不好
    TALLivePlayerErrorEndOfStream,//老师推流结束
};
typedef NS_ENUM(NSInteger, TALLivePlayerStatus) {
    TALLivePlayerStatusStarted = 0,
    TALLivePlayerStatusStartBuffering,
    TALLivePlayerStatusStopBuffering,
    TALLivePlayerStatusStopped,
    TALLivePlayerStatusVideoSize,
    TALLivePlayerStatusNetworkConnected,
    TALLivePlayerStatusFirstPacket,
};
typedef NS_ENUM(NSInteger, TALLivePlayerEvent) {
    // Generic error code
    TALLivePlayerEventUnknown = -1,
    TALLivePlayerEventSeriousBuffer = 0,//严重卡顿事件
    TALLivePlayerEventManualChangeLineSuccess,//手动切换线路成功
    TALLivePlayerEventManualChangeLineTimeout,//手动切换线路超时
    TALivePlayerEventReplayTimeout,//重连超时
    TALLivePlayerEventAutoChangeLineStart,//自动切换线路开始
    TALLivePlayerEventAutoChangeLineSuccess, //自动切换线路成功
    TALLivePlayerEventAutoChangeLineOverTimes,//自动切换线路前三个线路连接均失败
};
