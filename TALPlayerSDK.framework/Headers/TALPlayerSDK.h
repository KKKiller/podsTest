//
//  TALPlayerSDK.h
//  TALPlayerSDK
//
//  Created by 培优在线 on 2018/2/27.
//  Copyright © 2018年 培优在线. All rights reserved.
//

#import <UIKit/UIKit.h>
FOUNDATION_EXPORT double TALPlayerSDKVersionNumber;
FOUNDATION_EXPORT const unsigned char TALPlayerSDKVersionString[];
//! Project version number for TALPlayerSDK.

//! Project version string for TALPlayerSDK.

// In this header, you should import all the public headers of your framework using statements like #import <TALPlayerSDK/PublicHeader.h>

//#import "TALLivePlayer.h"
//#if __has_include(<TALPlayerSDK/TALLivePlayer.h>)
//
//
#import <TALPlayerSDK/TALLivePlayer.h>
#import <TALPlayerSDK/TALCommonDefine.h>
//#else
//#import "TALLivePlayer.h"
//#endif

