//
//  TALPlayerWrap.h
//  TALPlayerBirdge
//
//  Created by Sanji on 16/8/4.
//  Copyright © 2016年 Sanji. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <TALPlayerSDK/TALCommonDefine.h>
#import "TALCommonDefine.h"

typedef void(^EventCallback)(NSDictionary *msg);
typedef void(^PlayerCallback)(NSDictionary *dict);

// Codes that specify TALPlayer status.
typedef NS_ENUM(NSInteger, SDKMediaPlayerStatus) {
    SDKMediaPlayerStatusStarted = 0,
    SDKMediaPlayerStatusStartBuffering,
    SDKMediaPlayerStatusStopBuffering,
    SDKMediaPlayerStatusStopped,
    SDKMediaPlayerStatusVideoSize,
    SDKMediaPlayerStatusNetworkConnected,
    SDKMediaPlayerStatusFirstPacket,
};

extern NSString *const urlKey;
extern NSString *const lineIdKey;
extern NSString *const supplierKey;

@class TALLivePlayer;
@protocol TALLivePlayerDelegate <NSObject>
@optional
- (void)livePlayer:(TALLivePlayer *)player statusChanged:(TALLivePlayerStatus)status;
- (void)livePlayer:(TALLivePlayer *)player error:(TALLivePlayerError )error;

- (void)livePlayer:(TALLivePlayer *)player videoRate:(NSInteger)videoRate videoFps:(NSInteger)videoFps videoBufferTime:(NSInteger)videoBufferTime;
- (void)livePlayer:(TALLivePlayer *)player event:(TALLivePlayerEvent)event;

@end

@interface TALLivePlayer : NSObject

@property (nonatomic,copy,readonly)NSString*       currentlineId;          //!< 当前正在播放的线路Id
@property (nonatomic, assign) BOOL                 bufferReportEnable;     //!< 是否上报卡顿,根据课程状态设置
@property (nonatomic, assign) BOOL                 autoChangeLineEnable;   //!< 是否自动切换线路,根据课程状态设置
@property (nonatomic, copy) NSArray                *streamLines;           //!< 线路id

/**
 实例化播放器
 @param containerView 播放器视图
 @param streamLines 线路相关参数的数组，包括url、lineId、等，字典形式
 @param delegate 略
 */
- (instancetype)initWithContainerView:(UIView *)containerView streamLines:(NSArray *)streamLines delegate:(id<TALLivePlayerDelegate>)delegate;


/**
 根据URL播放视频

 @param url 直播地址
 @param lineId 直播地址唯一标志
 @param supplier 判定是否需要调度的参数
 */
- (void)playWithURL:(NSURL *)url lineId:(NSString *)lineId supplier:(NSString *)supplier;

/**
 根据streamId播放视频
 @param streamId 流id
 @param lineId 流唯一标志
 */
- (void)playWithStreamId:(NSString *)streamId lineId:(NSString *)lineId ;

/**
 手动切换线路根据url
 @param url 线路url
 @param lineId 线路唯一标志
 */
- (void)changeLineWithURL:(NSURL *)url lineId:(NSString *)lineId supplier:(NSString *)supplier;

/**
 手动切换线路根据streamId
 @param streamId 流Id
 @param lineId 流Id唯一标志
 */
- (void)changeLineWithStreamId:(NSString *)streamId lineId:(NSString *)lineId;

/**
 设置用户id
 @param userId 用户id
 */
- (void)setUserId:(NSString *)userId;

/**
 直播恢复
 @return 视频是否成功恢复
 */
- (BOOL)resume;

/**
 直播停止
 */
- (void)stop;

/**
 获取播放器是否播放
 @return 是否播放
 */
- (BOOL)isPlaying;

/**
 设置播放器静音
 @param mute 是否静音
 */
- (void)muteAudio:(BOOL)mute;

/**
 获取播放器是否静音
 @return 是否静音
 */
- (BOOL)isAudioMuted;

/**
 设置播放器音量
 @param volume 音量
 */
- (void)setAudioVolume:(NSInteger)volume;

/**
 播放器销毁
 */
- (void)destroy;
@end
