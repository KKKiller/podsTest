Pod::Spec.new do |s|

  s.name         = "zhouwukin_test"
  s.version      = "1.0.0"
  s.summary      = "微端NativeSDK"

  s.description  = "微端NativeSDK，基于TALMedia进行封装，支持native集成进行推拉流"

  s.homepage     = "https://gitlab.com/KKKiller/podsTest"
  s.license      = { :type => "MIT", :file => "LICENSE" }

  s.author       = { "zhouwukun" => "18678897939@163.com" }
  
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitlab.com/KKKiller/podsTest.git", :tag => "#{s.version}"  }

  s.vendored_frameworks = "TALPlayerSDK.framework","TALMedia.framework"


end
